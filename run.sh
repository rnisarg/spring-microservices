#!/bin/bash

declare project_dir=$(dirname $0)
declare dc_main=${project_dir}/docker-compose.yml

function start_all() {
    echo "Starting all services...."
    build_api
    echo $dc_main
    docker-compose -f docker-compose.yml up --build --force-recreate -d
    docker-compose -f docker-compose.yml  logs -f
}

function build_api() {
    mvn -f ${project_dir}/service-registry/pom.xml clean package -DskipTests
    sleep 2
    mvn -f ${project_dir}/cloud-gateway/pom.xml clean package -DskipTests
    sleep 2
    mvn -f ${project_dir}/hystrix-dashboard/pom.xml clean package -DskipTests
    sleep 2
    mvn -f ${project_dir}/department-service/pom.xml clean package -DskipTests
    sleep 2
    mvn -f ${project_dir}/user-services/pom.xml clean package -DskipTests
}

function stop_all() {
    echo 'Stopping all services....'
    docker-compose -f docker-compose.yml stop
    docker-compose -f docker-compose.yml rm -f
    docker rmi -f $(docker images -a -q)
}

function restart() {
    stop_all
    sleep 2
    start_all
}

action="start_all"

if [[ "$#" != "0"  ]]
then
    action=$@
fi

eval ${action}
