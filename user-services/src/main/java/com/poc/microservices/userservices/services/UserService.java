package com.poc.microservices.userservices.services;

import com.poc.microservices.userservices.domain.User;
import com.poc.microservices.userservices.dto.Department;
import com.poc.microservices.userservices.dto.ResponseTemplateVO;
import com.poc.microservices.userservices.repo.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    private final RestTemplate restTemplate;

    public UserService(UserRepository userRepository, RestTemplate restTemplate) {
        this.userRepository = userRepository;
        this.restTemplate = restTemplate;
    }


    public User saveUser(User user) {
        log.info("Inside saveUser of UserService");
        return userRepository.save(user);
    }

    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        log.info("Inside getUserWithDepartment of UserService");
        ResponseTemplateVO vo = new ResponseTemplateVO();
        User user = userRepository.findByUserId(userId);

        Department department = restTemplate.getForObject("http://192.168.99.100:9001/departments/" + user.getDepartmentId(), Department.class);
        //Department department = restTemplate.getForObject("http://localhost:9001/departments/" + user.getDepartmentId(), Department.class);


        vo.setUser(user);
        vo.setDepartment(department);

        return vo;
    }
}
