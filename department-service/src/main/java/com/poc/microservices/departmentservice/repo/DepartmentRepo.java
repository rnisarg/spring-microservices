package com.poc.microservices.departmentservice.repo;

import com.poc.microservices.departmentservice.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepo extends JpaRepository<Department, Long> {

    Department findByDepartmentId(Long departmentId);
}
